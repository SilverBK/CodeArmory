package com.bksilver.codearmory.collections

import scala.annotation.tailrec

sealed trait Tree[+T] {
  def getChildren: List[Tree[T]] = Nil

  def getSize(): Int = {
    @tailrec
    def loop(accumulator: Int, children: List[Tree[T]]): Int = children match {
      case Nil => accumulator
      case Empty() :: tail => loop(accumulator, tail)
      case x => loop(accumulator + 1, x.head.getChildren ::: x.tail)
    }

    loop(0, List(this))
  }
}

case class Empty() extends Tree[Nothing]

case class Leaf[T](value: T) extends Tree[T]

case class BinaryBranch[T](
  value: T,
  leftBranch: Tree[T],
  rightBranch: Tree[T]
) extends Tree[T] {
  override def getChildren: List[Tree[T]] = List(leftBranch, rightBranch)
}
