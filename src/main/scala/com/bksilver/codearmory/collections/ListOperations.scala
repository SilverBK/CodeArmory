package com.bksilver.codearmory.collections

import scala.annotation.tailrec

object ListOperations {
  def hasSubsequence[A](supper: List[A], sub: List[A]): Boolean = {
    @tailrec
    def loop(currentList: List[A], currentCheck: List[A]): Boolean = (currentList, currentCheck) match {
      case (_, Nil) => true
      case (x,y) if x.size < y.size => false
      case (xh :: _, yh :: _) if xh != yh => false
      case (x :: xs, y :: ys) if x == y => loop(xs, ys)
    }

    supper.sliding(sub.size).exists(loop(_, sub))
  }

  /**
    * Implementation of reversing a list
    *
    * @return A new reversed list
    */
  def reverse(l: List[Any]): List[Any] =
    l.foldLeft(List[Any]())((acc, next) => next :: acc)

  /**
    * Count a list of elements without using count, size or length
    */
  def count(l: List[Any]): Int =
    l.foldRight(0)((_, acc) => 1 + acc)
}
