package com.bksilver.codearmory.collections.trees.KDTree

/**
  * Node used to build the KDTree
  * @param value payload of the node
  * @param dimension Name of the dimension on the current level
  * @param left
  * @param right
  * @tparam T
  */
case class KDNode[T <: KDValue](
  value: T,
  dimension: String,
  left: KDNode[T],
  right: KDNode[T]
)
