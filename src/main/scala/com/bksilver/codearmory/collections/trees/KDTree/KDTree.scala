package com.bksilver.codearmory.collections.trees.KDTree

/**
  * Factory class for creating the KD-Tree
  */
object KDTreeFactory {
  def buildTree[T <: KDValue](col: Array[T]): KDNode[T] = {
    // Recursive builder
    def loop(depth: Int, branch: List[T]): KDNode[T] = {
      if(branch.isEmpty)
        null

      else {
        val nextDimension = getNextDimension(depth, branch.head)

        if (branch.length == 1)
          KDNode(branch.head, nextDimension, null, null)
        else {
          val ordered = branch.sortWith((a, b) => branch.head.getOrdering(a, b, nextDimension)).toList
          val medianIndex = ordered.length / 2
          val median = ordered(medianIndex)

          KDNode[T](
            value = median,
            dimension = nextDimension,
            left = loop(depth + 1, ordered.slice(0, medianIndex)),
            right = loop(depth + 1, ordered.slice(medianIndex + 1, ordered.length))
          )
        }
      }
    }

    KDTree
    loop(0, col.toList)
  }

  def getNextDimension[T <: KDValue](depth: Int, value: T): String = {
    val numberOfDimensions = this.getNumberOfDimensions(value)
    val target = depth % numberOfDimensions
    getDimensionName(value, target)
  }

  // Cache
  private var numberOfDimensions: Option[Int] = None
  private var mapping: Option[Array[String]] = None

  private def getNumberOfDimensions[T <: KDValue](value: T): Int =
    this.numberOfDimensions.getOrElse(value.declaredDimensions().length)

  private def getDimensionName[T <: KDValue](value: T, key: Int): String =
    this.mapping.getOrElse ( value.declaredDimensions () )( key )
}

case class KDTree[T <: KDValue](root: KDNode[T]) {
  /**
    *
    * @param neighbour        The node for which we are searching the nearest neighbour
    * @param distanceFunction The closer it is to 0 the better the match
    * @return
    */
  def findNearestNeighbour(neighbour: T,
                           distanceFunction: (Double, Double) => Double): KDNode[T] = {
    def loop(currentNode: KDNode[T]): KDNode[T] = {
      val sphere = HyperSphere(neighbour, currentNode.value, distanceFunction)

      val leftDistance =
        if(currentNode.left != null && sphere.isHyperPointWithin(currentNode.left.value))
          sphere.findSquaredDistanceToCenter(currentNode.left.value)
        else
          Double.MaxValue

      val rightDistance =
        if(currentNode.left != null && sphere.isHyperPointWithin(currentNode.right.value))
          sphere.findSquaredDistanceToCenter(currentNode.right.value)
        else
          Double.MaxValue

      if(leftDistance == Double.MaxValue && rightDistance == Double.MaxValue)
        currentNode
      else if(leftDistance < rightDistance)
        loop(currentNode.left)
      else
        loop(currentNode.right)
    }

    loop(root)
  }

  /**
    * Class representing a N-dimensional sphere. It is created by 2 points:
    * @param center of the hyper sphere
    * @param edgePoint located at the edge of the spere.
    */
  case class HyperSphere(center: T, edgePoint: T,
                         distanceFunction: (Double, Double) => Double) {
    val r_squared = findSquaredDistanceToCenter(edgePoint)

    /**
      * Check if a point in the N-dimensional space is located
      * in a N-dimensional sphere.
      * @param hyperPoint Poin in the N-dimensional space
      * @return True if it is within the sphere, false otherwise.
      */
    def isHyperPointWithin(hyperPoint: T): Boolean = {
      val distance_squared = findSquaredDistanceToCenter(hyperPoint)
      distance_squared < r_squared
    }

    /**
      * Get the distance from the center of the sphere to the point, however it is on 2nd power.
      * @param hyperPoint point in the N-dimensional space.
      * @return Sqared distance
      */
    def findSquaredDistanceToCenter(hyperPoint: T): Double =
      center.declaredDimensions().map(dim => {
        math.pow(distanceFunction(hyperPoint.getAsDouble(dim), center.getAsDouble(dim)), 2)
      }).sum
  }
}




class ThreeD(
  var x: Double,
  var y: Double,
  var z: Double
) extends KDValue(Map("x" -> x, "y" -> y, "z" -> z)) {
  /**
    * Get the mapping used to retrieve dimensions.
    *
    * @return Index -> Name mapping, the index shows the order in which the dimensions will be arranged.
    *         The name
    */
  override def declaredDimensions(): Array[String] = Array("x", "y", "z")

  override def getOrdering[T <: KDValue](prev: T, next: T, fieldName: String): Boolean = {
    classOf[ThreeD].getDeclaredFields.filter(_.getName == fieldName).map { field =>
      field.setAccessible(true)
      prev.getAsDouble(fieldName) > next.getAsDouble(fieldName)
    }.head
  }
}
