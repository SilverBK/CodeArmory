package com.bksilver.codearmory.collections.trees

import com.bksilver.codearmory.collections.trees.KDTree.ThreeD

import scala.io.Source

object DataLoadHelpers {
  def getTreeDElement(pathToCSV: String): Array[ThreeD] = {
    val bufferedLines = Source.fromFile(pathToCSV)
    val result = try {
      bufferedLines.getLines().map { line =>
        val cols = line.split(",").map(_.trim)

        if(cols(0).matches("[a-z]"))
          null
        else
          new ThreeD(x = cols(0).toDouble, y = cols(1).toDouble, z = cols(2).toDouble)
      }.filter(_ != null).toArray
    } finally {
      bufferedLines.close()
      Array[ThreeD]()
    }

    result
  }

}
