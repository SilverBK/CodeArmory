package com.bksilver.codearmory.collections.trees.KDTree

/**
  * Represents a value that can be used in the KDTree.
  * The fields that will be used in the tree MUST be of type Double currently.
  */
abstract class KDValue(val elements: Map[String, Any]) {

  /**
    * Get the dimensions used for three creation.
    *
    * @return a list of dimensions Index -> Name mapping, the index shows the order in which the dimensions will be arranged.
    *         The name
    */
  def declaredDimensions(): Array[String]

  def getOrdering[T <: KDValue](prev: T, next: T, fieldName: String): Boolean

  def getAsDouble(fieldName: String) = {
    this.elements(fieldName).asInstanceOf[Double]
  }
}

object KDValue {
  def filterRight(curr: KDValue, median: KDValue, fieldName: String): Boolean =
    curr.elements(fieldName).asInstanceOf[Double] >
      median.elements(fieldName).asInstanceOf[Double]

   def filterLeft(curr: KDValue, median: KDValue, fieldName: String): Boolean =
    curr.elements(fieldName).asInstanceOf[Double] <
      median.elements(fieldName).asInstanceOf[Double]
}
