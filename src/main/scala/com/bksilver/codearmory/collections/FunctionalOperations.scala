package com.bksilver.codearmory.collections

import scala.annotation.tailrec

object FunctionalOperations {
  def foldRight[A,B](l: List[A], z: B)( op: (A, B) => B): B = l match {
    case Nil => z
    case x :: xs => foldRight(xs, op(x, z))(op)
  }

  @tailrec
  def foldLeft[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
      case Nil => z
      case x :: xs => foldLeft(xs, f(z, x))(f)
    }

  //Reverse

  def flatten[A](l: List[List[A]]): List[A] =
    // We will use the built-in instead of our own.
    l.foldLeft(List[A]())(_ ::: _)
}
