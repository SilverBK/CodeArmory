package com.bksilver.codearmory.projectEuler

import com.bksilver.codearmory.math.Calculations

/**
  * Problem number 6 of project euler
  */
object SumSquareDifference {
  def solution(n: Int): Double = {
    val seq = (1 to n) map(_.toDouble) toList

    math.abs(Calculations.squareOfSum(seq) - Calculations.sumOfSquares(seq))
  }
}
