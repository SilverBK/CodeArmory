package com.bksilver.codearmory.math

import scala.annotation.tailrec
import scala.math.BigDecimal.RoundingMode.HALF_UP
import scala.math.ScalaNumber

object Calculations {
  /**
    * Calculate the sum of all squares in a list
    * @param l List of numbers
    */
  def sumOfSquares(l: List[Double]): Double =
    l.map(math.pow(_, 2)).sum

  /**
    * Calculate the square of the sum of all numbers in a list
    * @param l List of numbers
    */
  def squareOfSum(l: List[Double]): Double =
    math.pow(l.sum, 2)

  /**
    * Finds the factorial value of a given number
    */
  def factorialOf(number: Int): BigInt = {
    @tailrec
    def factorial(number: Int, acc: BigInt): BigInt = number match {
      case x if x < 0 => throw new IllegalArgumentException("number cannot be negative")
      case x if x == 0 => acc
      case _ => factorial(number - 1, acc * number)
    }

    factorial(number, 1)
  }

  /**
    * Generate a list of numbers contained in the factorial sequence
    *
    * @param to the number(included) up to which the sequence should be calculated
    * @return the sequence of factorial numbers
    */
  def generateFactorials(upperBound: Int): List[BigInt] =
    (0 to upperBound).foldLeft(List[BigInt]())((accumulator, index) =>
      if (accumulator.isEmpty) 1 :: accumulator
      else (index * accumulator.head) :: accumulator
    ).reverse // This is faster, because appending is an expensive operation

  /**
    * Calculate e to the power of x
    *
    * @param x         The power of e
    * @param terms     how many terms of the sequence to use in the calculation
    * @param precision set the precision of the calculations (digits after decimal point)
    */
  def eToX(x: Double, terms: Int = 10, precision: Int = 4): BigDecimal =
    generateFactorials(terms).zipWithIndex
      .map { case (factorial: BigInt, index: Int) =>
        if (index == 0) BigDecimal(1)
        else BigDecimal(math.pow(x, index)) / BigDecimal(factorial)
      }
      .sum
      .setScale(precision, HALF_UP)


  /*def generate(upperBound: Int): List[BigInt] = {
    (0 to upperBound)
      .foldRight(List[BigInt]())((index: Int, accumulator: List[BigInt]) => {
        if(index == 0 || index == 1) 1 :: accumulator
        else (accumulator.head + accumulator.tail.head) :: accumulator
      })
      */
}
