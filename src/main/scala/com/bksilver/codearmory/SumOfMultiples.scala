object SumOfMultiples {
  def sum(factors: Set[Int], limit: Int): Int =
    factors flatMap(getMultiples(_, limit)) sum

  def getMultiples(factor: Int, limit: Int): List[Int] =
    (factor until limit by factor).toList
}