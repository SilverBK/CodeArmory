package com.bksilver.codearmory.projectEuler

import org.scalatest.{FlatSpec, Matchers}


class SumSquareDifferenceTests extends FlatSpec with Matchers {
  SumSquareDifference.solution(3) shouldEqual 22

  SumSquareDifference.solution(10) shouldEqual 2640
}
