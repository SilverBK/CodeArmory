package com.bksilver.codearmory.collections

import org.scalatest.{FlatSpec, Matchers}
import com.bksilver.codearmory.collections.FunctionalOperations.{ foldRight, foldLeft, flatten }

class FunctionalOperationsTests extends FlatSpec with Matchers {
  "foldRight" should "behave as the default function" in {
    type myOp = (Int, Double) => Double
    val origin = List(1, 2, 10, 15, 33, 50)
    val z = 1
    val op: myOp = (cur, acc) => acc / cur

    val target = origin.foldRight[Double](z)(op)
    val actual = foldRight[Int, Double](origin, z)(op)

    target shouldEqual actual
  }

  "foldLeft" should "behave as the default function" in {
    type myOp = (Double, Int) => Double
    val origin = List(1, 2, 10, 15, 33, 50)
    val z = 1
    val op: myOp = (cur, acc) => acc / cur

    val target = origin.foldLeft[Double](z)(op)
    val actual = foldLeft[Int, Double](origin, z)(op)

    target shouldEqual actual
  }

  "foldLeft with deletion" should "be different than foldRight with deletion" in {
    type myOpL = (Double, Int) => Double
    type myOpR = (Int, Double) => Double
    val origin = List(1, 2, 10, 15, 33, 50)
    val z = 1
    val op: myOpL = (a, b) => a / b
    val opR: myOpR = (a, b) => a / b

    val leftResult = foldLeft[Int, Double](origin, z)(op)
    val rightResult = foldRight[Int, Double](origin, z)(opR)

    leftResult shouldNot equal(rightResult)
  }

  "flatten of [[1,2,3][4,5][6]]" should "be [1,2,3,4,5,6]" in {
    val origin = List(List(1,2,3), List(4,5), List(6))
    val target = List(1,2,3,4,5,6)

    flatten(origin) shouldEqual target
  }
}
