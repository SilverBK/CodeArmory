package com.bksilver.codearmory.collections

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.Matchers._

class ListOperationTests extends FlatSpec with Matchers{
    ListOperations.reverse(List(1,2,3,4)) should contain inOrder (4,3,2,1)

    ListOperations.count(List(1, 4, 5, 6, 7)) shouldEqual 5

    ListOperations.hasSubsequence(List(1,2,5,6,8), List(2,5)) shouldEqual true

    ListOperations.hasSubsequence(List(1,2,3,4,5), List(4,3)) shouldEqual false

    ListOperations.hasSubsequence(List(4,5,6,7,10,2), List(4,5,6,7)) shouldEqual true

    ListOperations.hasSubsequence(List(1,2,3), List(1,2,3)) shouldEqual true

    ListOperations.hasSubsequence(List(1,2,3), List(1,2,3,4)) shouldEqual false
}
