package com.bksilver.codearmory.collections

import com.bksilver.codearmory.collections.{ Tree, Leaf, BinaryBranch, Empty }
import org.scalatest.{FlatSpec, Matchers}

class TreeTests extends FlatSpec with Matchers {
  "tail recursive size" should "find the size of a leaf" in {
    val a = Leaf[Int](10)

    a.getSize() shouldEqual 1
  }

    "tail recursive size" should "find the size of a tree" in {
    val t = BinaryBranch(1, BinaryBranch(10, BinaryBranch(3, Leaf(5), Leaf(10)), BinaryBranch(7, Leaf(5), Leaf(7))), BinaryBranch(0, Leaf(9), Empty()))

    t.getSize() shouldEqual 10
  }
}
