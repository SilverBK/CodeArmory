package com.bksilver.codearmory.collections.trees

import com.bksilver.codearmory.collections.trees.KDTree.{KDTreeFactory, ThreeD}
import org.scalatest.{FlatSpec, Matchers}

class KDTreeTests extends FlatSpec with Matchers {
  val path = "/Users/Bozho/Source/CodeArmory/src/test/resources/pointdata.csv"
  val coordinates: Array[ThreeD] = DataLoadHelpers.getTreeDElement(path)

  val root = KDTreeFactory.buildTree(coordinates)


  "first element" should "be " in {

    root.value shouldNot equal(0)
  }

}
