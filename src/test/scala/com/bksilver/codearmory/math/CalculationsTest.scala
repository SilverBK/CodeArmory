package com.bksilver.codearmory.math

import org.scalatest.{FlatSpec, FunSpec, Matchers}
import com.bksilver.codearmory.math.Calculations.factorialOf
import com.bksilver.codearmory.math.Calculations.generateFactorials
import com.bksilver.codearmory.math.Calculations.eToX
import com.bksilver.codearmory.math.Calculations.sumOfSquares
import com.bksilver.codearmory.math.Calculations.squareOfSum

class CalculationsTest extends FlatSpec with Matchers {
  "Sum of squares of 1 to 10" should "be 385" in {
    sumOfSquares((1 to 10).map(_.toDouble).toList) shouldEqual 385
  }

  "Square of sum of 1 to 10" should "be 3025" in {
    squareOfSum((1 to 10).map(_.toDouble).toList) shouldEqual 3025
  }

  "factorial used with negative value" should "throw IllegalArgumentException" in {
    intercept[IllegalArgumentException] {
      factorialOf(-20)
    }
  }

  "Finding the nth factorial" should "work" in {
    factorialOf(0) shouldEqual 1

    factorialOf(1) shouldEqual 1

    factorialOf(5) shouldEqual 120

    factorialOf(18) shouldEqual BigInt("6402373705728000")
  }

  "Generation of factorial sequence" should "work" in {
    generateFactorials(1) shouldEqual List[BigInt](1, 1)

    generateFactorials(5) shouldEqual List[BigInt](1, 1, 2, 6, 24, 120)
  }

  "E to the X" should "work" in {
    eToX(20, 9) shouldEqual 2423600.1887

    eToX(0.5000) shouldEqual 1.6487

    eToX(-0.5000) shouldEqual 0.6065
  }
}
